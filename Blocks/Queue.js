class Queue {
  constructor(){
    this.memory = [];
    this.fptr = 0;
    this.lptr = 0;
  }

  push(data){
    this.memory[this.memory.length] = JSON.stringify(data);
    this.lptr = this.memory.length - 1;
  }

  pop(property = null){
    if(this.isEmpty()) return null;
    var data = this.memory[0];
    delete this.memory[0];
    this.trim();
    return data;
  }

  popLast(property = null){
    if(this.isEmpty()) return null;
    var data = this.memory[this.memory.length-1];
    delete this.memory[this.memory.length-1];
    this.trim();
    return data;
  }

  isEmpty(){
    return this.length() === 0;
  }

  trim(){
    this.memory = this.memory.filter((slot)=>{return slot!=null});
  }

  length(){
    return this.memory.filter((slot)=>{return slot!=null}).length;
  }
}
module.exports = Queue;
