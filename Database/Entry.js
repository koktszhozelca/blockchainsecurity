const moment = require("moment");
const Cryptographic = require("../Transaction/Cryptographic");

class Entry {
  constructor(data){
    this.slot = data;
    this.lastUpdate = null;
    this.checksum = null;
  }

  toJSON(){
    return {
      slot: this.slot,
      lastUpdate: this.lastUpdate,
      checksum: this.checksum
    }
  }

  _checksum(){
    if(this.lastUpdate === null) this.lastUpdate = moment().valueOf();
    this.checksum = Cryptographic.sha256(JSON.stringify(this.slot) + this.lastUpdate);
    return this;
  }

  _validate(){
    return this.checksum === Cryptographic.sha256(JSON.stringify(this.slot) + this.lastUpdate);
  }
}

module.exports = Entry;
